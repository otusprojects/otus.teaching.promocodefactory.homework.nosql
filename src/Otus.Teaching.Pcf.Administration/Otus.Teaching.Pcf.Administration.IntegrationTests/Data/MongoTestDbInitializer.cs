﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.DataAccess.MongoContext;

namespace Otus.Teaching.Pcf.Administration.IntegrationTests.Data
{
    public class MongoTestDbInitializer
    {
        private readonly IOptions<MongoDbSettings> _promoCodeDatabaseSettings;

        public MongoTestDbInitializer(IOptions<MongoDbSettings> promoCodeDatabaseSettings)
        {
            _promoCodeDatabaseSettings = promoCodeDatabaseSettings;
        }

        public void InitializeDb()
        {
            var mongoClient = new MongoClient(
                _promoCodeDatabaseSettings.Value.ConnectionString);

            mongoClient.DropDatabase(_promoCodeDatabaseSettings.Value.Database);
            var mongoDatabase = mongoClient.GetDatabase(
                _promoCodeDatabaseSettings.Value.Database);

            var preferenceCollection = mongoDatabase.GetCollection<Employee>(nameof(Employee));
            preferenceCollection.InsertMany(TestDataFactory.Employees);
        }

        public void CleanDb()
        {
            var mongoClient = new MongoClient(
                _promoCodeDatabaseSettings.Value.ConnectionString);

            mongoClient.DropDatabase(_promoCodeDatabaseSettings.Value.Database);
        }
    }
}