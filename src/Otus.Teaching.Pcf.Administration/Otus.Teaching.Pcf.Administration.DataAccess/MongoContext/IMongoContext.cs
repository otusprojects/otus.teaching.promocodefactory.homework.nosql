﻿using MongoDB.Driver;

namespace Otus.Teaching.Pcf.Administration.DataAccess.MongoContext
{
    public interface IMongoContext<T>
    {
        public IMongoCollection<T> Collection { get; }
    }
}