﻿using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain;
using Otus.Teaching.Pcf.Administration.DataAccess.MongoContext;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class MongoRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly IMongoContext<T> _context;

        public MongoRepository(IMongoContext<T> context)
        {
            _context = context;
        }

        public async Task AddAsync(T entity)
        {
            await _context.Collection.InsertOneAsync(entity);
        }

        public async Task DeleteAsync(T entity)
        {
            await _context.Collection.FindOneAndDeleteAsync(t => t.Id == entity.Id);
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var cursor = await _context.Collection.FindAsync(new BsonDocument());
            return await cursor.ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var cursor = await _context.Collection.FindAsync(t => t.Id == id);
            return await cursor.FirstOrDefaultAsync();
        }

        public async Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            var cursor = await _context.Collection.FindAsync(predicate);
            return await cursor.FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            var cursor = await _context.Collection.FindAsync(t => ids.Contains(t.Id));
            return await cursor.ToListAsync();
        }

        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            var cursor = await _context.Collection.FindAsync(predicate);
            return await cursor.ToListAsync();
        }

        public async Task UpdateAsync(T entity)
        {
            var update = new BsonDocument("$set", entity.ToBsonDocument());
            await _context.Collection.FindOneAndUpdateAsync(t => t.Id == entity.Id, update);
        }
    }
}
