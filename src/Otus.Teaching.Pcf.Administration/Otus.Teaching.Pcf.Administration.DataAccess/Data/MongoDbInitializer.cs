﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.DataAccess.MongoContext;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class MongoDbInitializer : IDbInitializer
    {
        private readonly IOptions<MongoDbSettings> _promoCodeDatabaseSettings;

        public MongoDbInitializer(IOptions<MongoDbSettings> promoCodeDatabaseSettings)
        {
            _promoCodeDatabaseSettings = promoCodeDatabaseSettings;
        }

        public void InitializeDb()
        {
            var mongoClient = new MongoClient(
                _promoCodeDatabaseSettings.Value.ConnectionString);

            mongoClient.DropDatabase(_promoCodeDatabaseSettings.Value.Database);
            var mongoDatabase = mongoClient.GetDatabase(
                _promoCodeDatabaseSettings.Value.Database);
            
            var booksCollection = mongoDatabase.GetCollection<Employee>(nameof(Employee));
            booksCollection.InsertMany(FakeDataFactory.Employees);
        }
    }
}
