﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.MongoContext
{
    public class MongoContext<T> : IMongoContext<T>
    {
        private readonly IMongoDatabase _db;

        public MongoContext(IOptions<MongoDbSettings> promoCodeDatabaseSettings)
        {
            var client = new MongoClient(promoCodeDatabaseSettings.Value.ConnectionString);
            _db = client.GetDatabase(promoCodeDatabaseSettings.Value.Database);
        }

        public IMongoCollection<T> Collection => _db.GetCollection<T>(typeof(T).Name);
    }
}
